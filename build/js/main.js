$(window).on('load', function () {
	$('body').removeClass('preload');
});

document.addEventListener('DOMContentLoaded', readyDom);

function readyDom() {
	//* меню в шапке
	(function () {
		$('.header__burger').on('click', function () {
			$(this)
				.toggleClass('active')
				.siblings('.header__nav')
				.toggleClass('active');

			$(this).parents('.header').toggleClass('active');
			$('body').toggleClass('locked');
		});
	})();

	//* открыть выпадающий список
	(function () {
		$('.open').on('click', function () {
			$(this).siblings('.dropdown').toggleClass('active');
			$(this).parents('.field').toggleClass('active');
		});

		$('.dropdown__hdr-close').on('click', function () {
			$('.dropdown').removeClass('active');
		});
	})();

	//* Подсвтека заполнего поля
	(function () {
		const fields = document.querySelectorAll('.js-field');
		fields.forEach((el) => {
			el.addEventListener('change', () => {
				if (el.value) {
					console.log(el.nextElementSibling);
					el.nextElementSibling.classList.add('full');
				} else {
					el.nextElementSibling.classList.remove('full');
				}
			});
		});
	})();

	//* Change lang
	(function () {
		$('.js-open_dropdown').click(function () {
			$(this)
				.toggleClass('active')
				.siblings('.js__dropdown')
				.toggleClass('show');
		});

		$(document).click(function (event) {
			if ($(event.target).closest('.js-wrap_dropdown').length) return;
			$('.js-open_dropdown').removeClass('active');
			$('.js__dropdown').removeClass('show');
		});
	})();
}
