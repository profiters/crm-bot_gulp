const vReg = new Vue({
	el: '.v-reg',
	data: {
		selected_step: 4,
		form: {
			company: {
				label: 'Название компании',
				type: 'text',
				value: '',
				error: '',
			},
			name: {
				label: 'ФИО',
				type: 'text',
				value: '',
				error: '',
			},
			phone: {
				label: 'Номер телефона',
				type: 'tel',
				value: '',
				error: '',
				mask: '+7 (___) ___-__-__',
				maxlength: 18,
				success: false,
			},
		},
		confirm: {
			value: '',
			error: '',
			open: false,
			codeLoad: false,
			timer: null,
			num: 90,
			success: false,
			serverHash: null,
			repeat: false,
			suppot: 0,
		},

		api_key: {
			value: '',
			error: '',
		},

		group: {
			value: '',
			error: '',
		},

		loading: false,
	},

	methods: {
		validation() {
			const errorMessages = {
				emptyError: 'Необходимо заполнить поле',
				phone: 'Не верно указан номер телефона',
			};

			Object.entries(this.form).forEach(([key, field]) => {
				if (!field.value) {
					this.form[key].error = errorMessages.emptyError;
					return;
				}

				if (field.maxlength && field.maxlength > field.value.length) {
					this.form[key].error = errorMessages[key];
				}
			});
		},

		clearError(key) {
			if (key == 'api_key') {
				this.api_key.error = '';
				return;
			}
			if (key == 'group') {
				this.group.error = '';
				return;
			}
			this.form[key].error = '';
		},

		maskPhone(key, event) {
			const keyCode = event.keyCode;
			const template = this.form[key].mask;

			if (!template) return;

			const def = template.replace(/\D/g, '');
			const val = this.form[key].value.replace(/\D/g, '');

			let i = 0;

			let newValue = template.replace(/[_\d]/g, function (a) {
				return i < val.length ? val.charAt(i++) || def.charAt(i) : a;
			});

			i = newValue.indexOf('_');

			if (i !== -1) {
				newValue = newValue.slice(0, i);
			}

			let reg = template
				.substr(0, this.form[key].value.length)
				.replace(/_+/g, function (a) {
					return '\\d{1,' + a.length + '}';
				})
				.replace(/[+()]/g, '\\$&');

			reg = new RegExp('^' + reg + '$');

			if (
				!reg.test(this.form[key].value) ||
				this.form[key].value.length < 5 ||
				(keyCode > 47 && keyCode < 58)
			) {
				this.form[key].value = newValue;
			}
		},

		toEnd(key, event) {
			const template = this.form[key].mask;
			if (!template) return;
			event.target.selectionStart = event.target.value.length;
		},

		nextStep() {
			if (this.api_key.value == '') {
				this.api_key.error = 'Необходимо заполнить поле';
				return;
			}

			this.selected_step = 3;
		},
		lastStep() {
			if (this.group.value == '') {
				this.group.error = 'Необходимо заполнить поле';
				return;
			}

			this.selected_step = 4;
		},

		confirmPhone() {
			this.validation();

			if (!this.formIsValid) return;

			// this.loading = true;

			const data = {
				company: this.form.company.value,
				name: this.form.name.value,
				phone: this.form.phone.value.replace(/[^+\d]+/g, ''),
			};

			console.log(data);

			this.form.phone.success = true;

			this.selected_step = 2;

			// this.confirm.value = '';
			// this.confirm.error = '';
			// this.confirm.success = false;
			// this.confirm.suppot++;

			// QAjax('CompClientAuth.authorizeStart', data, (response) => {
			// 	if (response.status != 200) return;

			// 	this.confirm.serverHash = response.result.hash;
			// 	this.confirm.codeLoad = true;
			// 	this.confirmTimer();

			// 	Vue.nextTick(() => {
			// 		this.$refs.confirmField.focus();
			// 	});
			// 	this.loading = false;
			// });
		},
		// validationCodeConfirm() {
		// 	const clientHash = CryptoJS.SHA256('' + this.confirm.value).toString();
		// 	const serverHash = this.confirm.serverHash;

		// 	const data = {
		// 		phone: this.phoneValue,
		// 		hash: clientHash,
		// 	};

		// 	if (this.confirm.value.length > 3) {
		// 		if (clientHash === serverHash) {
		// 			this.confirm.success = true;
		// 			this.$refs.confirmAction.classList.add('hide');
		// 			this.$refs.confirmPhone.classList.add('hide');

		// 			setTimeout(() => {
		// 				this.loading = true;
		// 			}, 1000);

		// 			setTimeout(() => {
		// 				QAjax('CompClientAuth.authorizeConfirm', data, (response) => {
		// 					if (response.status != 200) return;
		// 					this.phoneValue = response.client.phone;
		// 					this.confirm.loading = false;

		// 					this.$emit('complete', this.confirm.success);
		// 				});
		// 			}, 1600);
		// 		} else {
		// 			this.confirm.error = 'Не верный код';
		// 		}
		// 	} else {
		// 		this.confirm.error = '';
		// 	}

		// 	if (this.confirm.value.length > 4) {
		// 		this.confirm.value = this.confirm.value.slice(0, 4);
		// 	}
		// },

		// confirmTimer() {
		// 	this.confirm.num--;
		// 	if (this.confirm.num <= 0) {
		// 		clearTimeout(this.confirm.timer);
		// 		this.confirm.repeat = false;
		// 		this.confirm.num = 90;
		// 		this.confirm.codeLoad = false;
		// 	} else {
		// 		this.confirm.repeat = true;
		// 		timer = setTimeout(this.confirmTimer, 1000);
		// 	}
		// },
	},

	computed: {
		formIsValid() {
			return Object.values(this.form).every((el) => el.error === '');
		},
	},
});
